require('dotenv').config();
require('./utils').initOs();
const TelegramBot = require('node-telegram-bot-api');
const token = process.env.DEBUG === 'true' ? process.env.LOCAL_TOKEN : process.env.SERVER_TOKEN;

const bot = new TelegramBot(token, {polling: true});
const constant = require('./const');
const db = require('./db');
const utils = require('./utils');
const send = require('./ui/message');
const step = require('./steps');
const command = require('./ui/command');
const moment = require('moment-timezone');
const validation = require('./validation');
const financial = require('./financial');


bot.on('message', (msg) => {
    console.log('Message with Teleid : ' + msg.chat.id + " And Chat : " + msg.text);
    db.initUser(msg);
    if (msg.text.includes("start")) send.startMessage(bot, msg, 'لطفا از طریق منو گزینه درخواستی را انتخاب کنید.');
    else if (msg.text.includes("وجه تضمین")) getDeposit(msg);
    else if (msg.text.includes("امروزی")) orderValidation(msg, constant.today);
    else if (msg.text.includes("فردایی")) orderValidation(msg, constant.tomorrow);
    else if (msg.text.includes("گزارش یک ماهه")) chartOneMonth(msg);
    else if (msg.text.includes("تسویه موقت")) clearing(msg);
    else if (msg.text.includes("وجه سود یا ضرر")) getAmount(msg);
    else if (msg.text.includes("بازگشت به منو اصلی")) send.startMessage(bot, msg, 'لطفا از طریق منو گزینه درخواستی را انتخاب کنید.');
    else if (msg.text.includes("راهنما")) help(msg);
    else if (msg.text.includes("ویرایش مشخصات")) send.editProfileMessage(bot, msg, 'لطفا از طریق منو گزینه درخواستی برای ویرایش لطلاعات را انتخاب کنید.');
    else if (msg.text.includes("خرید")) buy(msg);
    else if (msg.text.includes("فروش")) sell(msg);
    else if (msg.text.includes("نام کوچک")) firstName(msg);
    else if (msg.text.includes("نام خانوادگی")) lastName(msg);
    else if (msg.text.includes('شماره موبایل')) menuPhone(msg);
    else if (msg.text.includes('شماره حساب')) menuCredit(msg);
    else if (msg.text.includes('خروج از مدیریت')) send.startMessage(bot, msg, 'لطفا از طریق منو گزینه درخواستی را انتخاب کنید.');

    else if (msg.text.includes('mahan7797')) goToAdmin(msg);
    else if (msg.text.includes('ثبت قیمت نهایی و محاسبه کل سود و ضرر ها 📃')) admin_calculateAddAmount(msg);
    else if (msg.text.includes('فرستادن پیغام به همه کاربران 📃')) snedMessageToall(msg);
    else elseMessage(msg);


});

bot.on('callback_query', function onCallbackQuery(msg) {
    if (msg.data.startsWith("accept")) command.accept(bot, msg);
    else if (msg.data.startsWith("edit")) command.edit(bot, msg);
    else if (msg.data.startsWith("destroy")) command.destroy(bot, msg);

});


async function snedMessageToall(msg) {
    db.updateCommand(msg, constant.broadcast)
    send.simpleMessage(bot, msg, 'لطفا متن مورد نظر برای فرستادن به همه کابران را ارسال کنید.');
}

async function getDeposit(msg) {

    let user = await db.findUserByid(msg);
    db.updateCommand(msg, constant.start)
    send.simpleMessage(bot, msg, user.deposit + " تومان ");




}

async function getAmount(msg) {
    let user = await db.findUserByid(msg);
    db.updateCommand(msg, constant.start);
    send.simpleMessage(bot, msg, user.amount + " تومان ")

}

function admin_calculateAddAmount(msg) {
    db.updateCommand(msg, constant.admin);
    send.simpleMessage(bot, msg, 'لطفا مقدار نهایی دلار را وارد کرده و تایید را بزنید')
}


function chartOneMonth(msg) {
    db.updateCommand(msg, constant.start);
    bot.sendMessage(msg.chat.id, 'گزارش یک ماهه')
}

async function clearing(msg) {
    db.updateCommand(msg, constant.start);
    const amount = await validation.checkoutValidation(bot, msg)
    await db.createCheckout(msg.chat.id, amount)
    db.updateAmount(msg.chat.id, 0)
}

function help(msg) {
    db.updateCommand(msg, constant.start);
    send.helpMessage(bot, msg)
}

async function buy(msg) {
    let status = await db.getCommand(msg);
    if (status === constant.today)
        await createTodayBuy(msg);
    else if (status === constant.tomorrow)
        await createTomorrowBuy(msg);
}

async function sell(msg) {
    let status = await db.getCommand(msg);
    if (status === constant.today)
        await createTodaySell(msg);
    else if (status === constant.tomorrow)
        await createTomorrowSell(msg);


}

function firstName(msg) {

    db.updateCommand(msg, constant.fname);
    send.backMessage(bot, msg, 'لطفا نام کوچک خود را وارد کنید و کلید ارسال را بزنید')


}

function lastName(msg) {
    db.updateCommand(msg, constant.lname);
    send.backMessage(bot, msg, 'لطفا نام خانوادگی خود را وارد کنید و کلید ارسال را بزنید')
}

function menuCredit(msg) {
    db.updateCommand(msg, constant.credit);
    send.backMessage(bot, msg, 'لطفا شماره حساب خود را وارد کنید و کلید ارسال را بزنید');
}

function menuPhone(msg) {
    db.updateCommand(msg, constant.phone);
    send.backMessage(bot, msg, 'لطفا شماره تلفن  خود را وارد کنید و کلید ارسال را بزنید')
}


async function elseMessage(msg) {

    let status = await db.getCommand(msg);
    msg.text = utils.fa2en(msg.text);
    if (status === constant.fname) {
        db.changeFirstName(msg);
        send.editProfileMessage(bot, msg, 'نام کوچک با موفقیت ثبت شد')
    }
    else if (status === constant.lname) db.changeLastName(msg).then(() => send.editProfileMessage(bot, msg, 'نام خانوادگی با موفقیت ثبت شد'));
    else if (status === constant.phone) db.changePhone(msg).then(() => send.editProfileMessage(bot, msg, 'شماره تلفن با موفقیت ثبت شد'));
    else if (status === constant.credit) db.changeCredit(msg).then(() => send.editProfileMessage(bot, msg, 'شماره حساب با موفقیت ثبت شد'));
    else if (status === constant.todayBuyStep1) step.step1(bot, msg, constant.buy, true, constant.todayBuyStep2);
    else if (status === constant.todayBuyStep2) step.step2(bot, msg, constant.buy, true);
    else if (status === constant.todaySellStep1) step.step1(bot, msg, constant.sell, true, constant.todaySellStep2);
    else if (status === constant.todaySellStep2) step.step2(bot, msg, constant.sell, true);
    else if (status === constant.tomorrowBuyStep1) step.step1(bot, msg, constant.buy, false, constant.tomorrowBuyStep2);
    else if (status === constant.tomorrowBuyStep2) step.step2(bot, msg, constant.buy, false);
    else if (status === constant.tomorrowSellStep1) step.step1(bot, msg, constant.sell, false, constant.tomorrowSellStep2);
    else if (status === constant.tomorrowSellStep2) step.step2(bot, msg, constant.sell, false);
    else if (status === constant.countUpdate) command.editStep2(bot, msg);
    else if (status === constant.feeUpdate) command.editFinal(bot, msg);
    else if (status === constant.admin) financial.doFinancialJob(bot, msg, msg.text)
    else if (status === constant.broadcast) broadcastMessageToAll(msg)


}

async function broadcastMessageToAll(msg) {
    let users = await db.listofAllUser()
    for (let i = 0; i < users.length; i++) {
        send.simpleMessage(bot, {chat: {id: users[i].teleid}}, msg.text)
    }

    send.simpleMessage(bot, msg, 'پیغام با موفقیت ارسال شد.')
}

function createTodayBuy(msg) {
    db.updateCommand(msg, constant.todayBuyStep1);
    send.backMessage(bot, msg, 'لطفا مقدار دلار مورد نیاز خود را  وارد کنید و کلید ارسال را بزنید');


}

function createTodaySell(msg) {
    db.updateCommand(msg, constant.todaySellStep1);
    send.backMessage(bot, msg, 'لطفا مقدار دلار  خود را برای فروش را وارد کنید و کلید ارسال را بزنید');


}

function goToAdmin(msg) {
    db.updateCommand(msg, constant.admin);
    send.adminPanel(bot, msg, 'به قسمت مدیریت ربات خوش آمدید');


}

function createTomorrowBuy(msg) {
    db.updateCommand(msg, constant.tomorrowBuyStep1);
    send.backMessage(bot, msg, 'لطفا مقدار دلار مورد نیاز خود را برای خرید وارد کنید و کلید ارسال را بزنید')


}

function createTomorrowSell(msg) {
    db.updateCommand(msg, constant.tomorrowSellStep1);
    send.backMessage(bot, msg, 'لطفا مقدار دلار مورد نیاز خود را برای فروش وارد کنید و کلید ارسال را بزنید')


}

async function orderValidation(msg, type) {

    const user = await db.findUserByid(msg);


    await detailValidation(msg, user);

    await depositValidation(msg, user);

    await timeValidation(msg);

    send.todayTomorrowMessage(bot, msg, type)


}

function geHour() {
    var day = moment().tz(constant.tehran);

    return day.format("H");

}

function getMinute() {
    var day = moment().tz(constant.tehran);

    return day.format("m");

}


async function detailValidation(msg, user) {
    let text;
    if (user.custom_fname === null || user.custom_lname === null || user.phone === null || user.credit === null) {
        text = '📌 لطفا اطلاعات زیر را وارد کنید📌';
        text += '\n';
        text += user.custom_fname === null ? 'نام\n' : '';
        text += user.custom_lname === null ? 'نام خانوادگی\n' : '';
        text += user.phone === null ? 'شماره موبایل\n' : '';
        text += user.credit === null ? 'شماره حساب\n' : '';
        send.editProfileMessage(bot, msg, text);
        throw 'user not fill all details'

    }


}

async function depositValidation(msg, user) {
    if (user.deposit === 0) {
        send.simpleMessage(bot, msg, 'وجه اعتبار شما کافی نیست و اجازه شرکت در اتاق معاملات را ندارید');
        throw 'not enough deposit'
    }


    else if (user.amount < 0) {
        send.simpleMessage(bot, msg, 'تا زمان تصفیه وجه ضرری خود امکان شرکت در اتاق معاملات را ندارید');
        throw 'checkout zarar'
    }


}

async function timeValidation(msg) {

    let text;
    if (geHour() >= 10 && geHour() < 21) {
        if (geHour() >= 10 && geHour() <= 13) {
            if (geHour() === 13) {
                if (getMinute() <= 30)
                    return true;
            }
            else {
                return true
            }

        }

        else {
            if (msg.text.includes("امروزی")) {
                text = "📌امکان ثبت معامله امروزی وجود ندارد.";
                text += "\n";
                text += 'توجه شود بازه زمانی ثبت معامله امروزی بین ساعت 10 تا 13:30 می باشد.';
                text += '\n';
                text += 'برای ثبت معامله لطفا معامله فردایی را انتخاب کنید.';

                send.simpleMessage(bot, msg, text);
                throw 'can not submit today'
            }
            else return true
        }


    }
    else {
        text = "📌امکان ثبت معامله امروزی  و فردایی وجود ندارد.";
        text += "\n";
        text += 'توجه شود بازه زمانی ثبت معامله امروزی و فردایی  بین ساعت 10 تا 21 می باشد.';
        text += '\n';
        text += 'برای ثبت معامله لطفا ساعت 10 اقدام کنید.';
        send.simpleMessage(bot, msg, text);
        throw 'can not  submit today ad tomorrow'
    }

}












