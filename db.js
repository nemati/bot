const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const constant = require('./const');
const moment = require('moment-timezone');
const utils = require('./utils');
const sequelize = new Sequelize('node', 'root', 'mahan7797', {
    define: {
        charset: 'utf8',
        collate: 'utf8_general_ci'
    },
    host: '172.17.0.1',
    dialect: 'mysql',
    logging: false,




    operatorsAliases: false
});


const User = sequelize.define('user', {
    teleid: Sequelize.INTEGER,
    username: Sequelize.STRING,
    first_name: Sequelize.STRING,
    last_name: Sequelize.STRING,
    custom_fname: Sequelize.STRING,
    custom_lname: Sequelize.STRING,
    edit_request: Sequelize.STRING,
    type: Sequelize.STRING,
    phone: Sequelize.STRING,
    credit: Sequelize.STRING,
    usages: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 0},
    tmp_tod: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 0},
    tmp_tom: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 0},
    command: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 0},
    amount: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 0},
    deposit: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 0},
    is_bot: Sequelize.BOOLEAN
});


const Request = sequelize.define('request', {
    teleid_request: Sequelize.INTEGER,
    fee: Sequelize.DOUBLE,
    count: Sequelize.DOUBLE,
    teleid_accept: Sequelize.DOUBLE,
    is_today: Sequelize.BOOLEAN,
    start_time: Sequelize.STRING,
    type: Sequelize.STRING,
    final: Sequelize.DOUBLE,
    edit: {type: Sequelize.BOOLEAN, allowNull: false, defaultValue: false},
    status: {type: Sequelize.STRING, allowNull: false, defaultValue: 'pending'}


});

const MessageRequest = sequelize.define('mreq', {
    teleid: Sequelize.INTEGER,
    message_id: Sequelize.INTEGER,
    request_id: Sequelize.DOUBLE,
    owner: Sequelize.BOOLEAN,
    status: {type: Sequelize.STRING, allowNull: false, defaultValue: 'open'}


});

const FinalPriceDay = sequelize.define('final_price_day', {
    price: Sequelize.DOUBLE,
    day: Sequelize.STRING

});

const Checkout = sequelize.define('checkout', {
    teleid: Sequelize.INTEGER,
    amount: Sequelize.DOUBLE,
    time: Sequelize.STRING,
    status: {type: Sequelize.STRING, allowNull: false, defaultValue: 'open'}

});




async function createMessageRequest(data) {

    let mRequest = await MessageRequest.create({
        teleid: data.message.chat.id,
        message_id: data.message.message_id,
        request_id: data.request.id,
        owner: data.message.chat.id === data.request.teleid_request

    })
    return mRequest;


}

function createCheckout(teleid, amount) {
    return new Promise(function (resolve, reject) {
        Checkout.create({
            teleid: teleid,
            amount: amount,
            time: moment().tz(constant.tehran).format()


        }).then(task => {
            resolve(task)
        })
    });

}


function initUpdate(user, msg) {

    user.username = msg.chat.username;
    user.first_name = msg.chat.first_name;
    user.last_name = msg.chat.last_name;
    user.type = msg.chat.type;
    user.save();


}

function initUser(msg) {

    return new Promise(function (resolve, reject) {
        User
            .findOrCreate({where: {teleid: msg.chat.id}})
            .spread((user, created) => {
                user.username = msg.chat.username;
                user.first_name = msg.chat.first_name;
                user.last_name = msg.chat.last_name;
                user.type = msg.chat.type;
                user.usages = user.usages + 1;
                user.save();
                resolve(user)
            })

    });
}



function updateDepositWithZaraz(teleid,zaraz) {

    return new Promise(function (resolve, reject) {
        User
            .findOrCreate({where: {teleid: teleid}})
            .spread((user, created) => {
                if (zaraz > user.deposit)
                    resolve(true)
                else {
                    user.deposit=user.deposit - zaraz
                    user.save();
                    resolve(user)

                }

            })

    });
}

function findUserByid(msg) {


    return new Promise(function (resolve, reject) {
        User.findOne({where: {teleid: typeof msg.chat !== 'undefined' ? msg.chat.id : msg.message.chat.id}})
            .then((data) => resolve(data))
    })
}

async function findRequestByid(msg) {
    return await Request.findOne({where: {id: typeof msg.data !== 'undefined' ? msg.data : msg.edit_request.id}})
}

function canOrderDependDeposit(msg, edit) {


    return new Promise(function (resolve, reject) {
        User.findOne({raw: true, where: {teleid: typeof msg.chat !== 'undefined' ? msg.chat.id : msg.message.chat.id}})
            .then((data) => {

                var total;
                total = count * 1000;

                if (utils.checkUserCanTransaction(data, total, type))
                    resolve(true);
                else
                    reject(false)
            })
    })
}

async function canOrderDependDepositWithoutRequest(msg, count, type) {


    let user = await User.findOne({
        raw: true,
        where: {teleid: getTeleid(msg)}
    });


    var total;
    total = count * 1000;

    if (!utils.checkUserCanTransaction(user, total, type))
        throw 'Can not be Transtaction'
    return true;


}


async function changeFirstName(msg) {

    let user = await User.findOne({
        where: {teleid: getTeleid(msg)}
    });

    user.custom_fname = msg.text;
    await user.save();
    return user;
}

async function changeLastName(msg) {

    let user = await User.findOne({
        where: {teleid: getTeleid(msg)}
    });

    user.custom_lname = msg.text;
    await user.save();
    return user;

}

async function changePhone(msg) {

    let user = await
        User.findOne({
            where: {teleid: getTeleid(msg)}
        });
    user.phone = msg.text;
    await user.save();
    return user;

}

async function changeCredit(msg) {

    let user = await User.findOne({
        where: {teleid: msg.chat.id},
    });
    user.credit = msg.text;
    await user.save();
    return user;

}

async function changeStatusRequest(msg, status) {

    let request = await Request.findOne({
        where: {id: msg.data},
    });
    request.status = status;
    if (status === constant.close_on)
        request.teleid_accept = msg.message.chat.id;
    await request.save();
    return request;


}


function getMultipleUser(msg) {
    return new Promise(function (resolve, reject) {
        var requesterUser = {
            chat:
                {
                    id: msg.teleid_request
                }
        };
        var accepterUser = {
            chat:
                {
                    id: msg.teleid_accept
                }
        };
        var data;


        findUserByid(requesterUser).then((user1) => {

            return findUserByid(accepterUser).then((user2) => {
                data = {
                    requester: user1.dataValues,
                    accepter: user2.dataValues
                };

                resolve(data)
            })
        })

    });


}


async function updateCommand(msg, command) {

    let user = await User.findOne({
        where: {teleid: typeof msg.chat !== 'undefined' ? msg.chat.id : msg.message.chat.id},
    })
    user.command = command;
    user.save();
    return user;


}


async function addTempValue(msg, type, isToday, amount) {

    let user = await User.findOne({
        where: {teleid: typeof msg.chat !== 'undefined' ? msg.chat.id : msg.message.chat.id}
    });

    if (type === constant.sell)
        amount = amount * -1;
    if (isToday || (geHour() >= 15 && geHour() < 23))
        user.tmp_tod += amount;
    else
        user.tmp_tom += amount;
    await user.save();
    return user;


}

function revertTempValue(msg, request) {
    return new Promise(function (resolve, reject) {
        User.findOne({
            where: {teleid: request.teleid_request},
        }).then(project => {
            var amount = request.count * 1000;
            if (request.type === constant.buy)
                amount = amount * -1;
            if (request.is_today || (geHour(request.start_time) >= 15 && geHour(request.start_time) < 23))
                project.tmp_tod += amount;
            else
                project.tmp_tom += amount;
            project.save();
            resolve(project)
        })

    });


}


function geHour(time) {

    var day;
    if (time === 'undefined')
        day = moment().tz(constant.tehran);
    else
        day = moment(time).tz(constant.tehran);

    return day.format("H");

}

function updateUserRequest(msg, edit) {
    return new Promise(function (resolve, reject) {
        User.findOne({
            where: {teleid: typeof msg.chat !== 'undefined' ? msg.chat.id : msg.message.chat.id},
        }).then(project => {
            project.edit_request = edit;
            project.save();
            resolve(project)
        })

    });


}

function updateAmount(teleid, amount) {
    return new Promise(function (resolve, reject) {
        User.findOne({
            where: {teleid: teleid},
        }).then(project => {
            project.amount = amount;
            project.save();
            resolve(project)
        })

    });


}


function getAmount(msg) {
    return new Promise(function (resolve, reject) {
        User.findOne({
            where: {teleid: msg.chat.id},
        }).then(project => {

            resolve(project)
        })

    });
}

function getDeposit(msg) {
    return new Promise(function (resolve, reject) {
        User.findOne({
            raw: true,
            where: {teleid: typeof msg.chat !== 'undefined' ? msg.chat.id : msg.message.chat.id},
        }).then(project => {

            resolve(project.deposit)
        })

    });
}


function getCommand(msg) {
    return new Promise(function (resolve, reject) {
        User.findOne({
            where: {teleid: msg.chat.id},
        }).then(project => {
            resolve(project.command)
        })
    });
}

async function createNewRequest(msg, kind, isToday) {

    return await Request.create({
        teleid_request: msg.chat.id,
        type: kind,
        count: utils.fa2en(msg.text),
        is_today: isToday

    })


}

function updateRequest(msg, kind, isToday) {
    return new Promise(function (resolve, reject) {

        Request.findOne({
            order: [
                ['id', 'DESC']
            ],
            where: {teleid_request: msg.chat.id, type: kind, is_today: isToday},
        }).then(project => {
            project.fee = utils.fa2en(msg.text);
            project.start_time = moment().tz(constant.tehran).format();
            project.save();
            resolve(project.dataValues)
        })
    });


}

async function getLastRequest(msg, kind, isToday) {


    let request = await Request.findOne({
        raw: true,
        order: [
            ['id', 'DESC']
        ],
        where: {teleid_request: msg.chat.id, type: kind, is_today: isToday},
    });
    return request;


}

function updateRequestFinal(requestId, status, amount) {
    return new Promise(function (resolve, reject) {

        Request.findOne({

            where: {id: requestId},
        }).then(request => {
            request.final = amount
            request.status = status;
            request.save();
            resolve(request)
        })
    });


}

function updateAmountUser(teleid, amount) {
    return new Promise(function (resolve, reject) {

        User.findOne({

            where: {teleid: teleid},
        }).then(request => {

            request.amount += amount;
            request.save();
            resolve(request.dataValues)
        })
    });


}


async function updateRequestByCountAndFee(user) {

    return await Request.update(
        {
            count: user.edit_request.count,
            fee: user.edit_request.fee,
            edit: true
        },
        {where: {id: user.edit_request.id}});

}

async function updateMessageRequesterSender(id, newMessageId) {

    return await MessageRequest.update(
        {
            message_id: newMessageId
        },
        {where: {id: id}});

}


async function getFilterdUserByRequest(msg, request, type) {

    var amount = 1000 * request.count;
    var q;
    var teleid = msg.chat !== 'undefined' ? msg.chat.id : msg.message.chat.id;
    if (type === constant.buy) {
        amount = amount * -1;
        q = `SELECT id, teleid, username, first_name, last_name, custom_fname, custom_lname, edit_request, type, phone, credit, tmp_tod, tmp_tom, command, amount,  deposit, is_bot, createdAt, updatedAt FROM users AS user WHERE (tmp_tom + tmp_tod + ${amount}) >= (-1*deposit) and teleid != ${teleid} and amount >= 0`
    }

    else {
        q = `SELECT id, teleid, username, first_name, last_name, custom_fname, custom_lname, edit_request, type, phone, credit, tmp_tod, tmp_tom, command, amount,  deposit, is_bot, createdAt, updatedAt FROM users AS user WHERE (tmp_tom + tmp_tod + ${amount}) <= deposit and teleid != ${teleid} and amount >= 0`
    }

    let users = await sequelize.query(q, {model: User})

    return users;


}

async function getMessageRequest(request) {


    return await MessageRequest.findAll({

        where: {
            request_id: request.id
        },
    })


}


async function listofAllUser() {


    return await MessageRequest.findAll({

    })


}

function getAllMessageRequest(msg) {
    return new Promise(function (resolve, reject) {
        MessageRequest.findAll({
            raw: true,
            where: {request_id: msg.data},
        }).then(project => {

            resolve(project)
        })

    });

}

function getAllRequestByStatus(status) {
    return new Promise(function (resolve, reject) {
        Request.findAll({
            raw: true,
            where: {status: status},
        }).then(project => {
            resolve(project)
        })

    });

}

function resetTemp() {
    return new Promise(function (resolve, reject) {
        var q = 'UPDATE users as main INNER JOIN users AS inc ON main.id = inc.id set main.tmp_tod = inc.tmp_tom,main.tmp_tom=0';

        sequelize.query(q, {})

            .then(() => {
                resolve(true)
            })

    });

}


function updateStatusRequest(msg, status) {
    return new Promise(function (resolve, reject) {
        MessageRequest.update(
            {status: status},
            {where: {request_id: msg.data}})
            .then(() => resolve(true))


    });

}


function getFinalPricePerDay(day) {
    return new Promise(function (resolve, reject) {
        FinalPriceDay.findOne({where: {day: day}})
            .then((data) => resolve(data))
    });
}

function addOrUpdateFinacialDay(day, price) {
    return new Promise(function (resolve, reject) {
        FinalPriceDay
            .findOrCreate({where: {day: day}})
            .spread((user, created) => {
                user.price = price;
                user.save();
                resolve(true)
            })

    });
}

function getPriceByDay(day) {
    return new Promise(function (resolve, reject) {
        FinalPriceDay.findOne({
            raw: true,
            where: {day: day}
        })
            .then((data) => resolve(data.price))
    })
}

function getAllActiveRequest() {
    return new Promise(function (resolve, reject) {

        Request.findAll({
            raw: true,
            where: {
                start_time: {
                    [Op.ne]: null
                },

                status: constant.pending


            },
        }).then(project => {

            resolve(project)
        })
    });


}

function getTeleid(msg) {
    if (typeof msg.chat !== 'undefined')
        return msg.chat.id;
    else if (typeof msg.message !== 'undefined')
        return msg.message.chat.id;
    else return msg;
}


module.exports = {
    initUser: initUser,
    changeFirstName: changeFirstName,
    changeLastName: changeLastName,
    changePhone: changePhone,
    changeCredit: changeCredit,
    updateRequest: updateRequest,
    updateCommand: updateCommand,
    getAmount: getAmount,
    getCommand: getCommand,
    createNewRequest: createNewRequest,
    findUserByid: findUserByid,
    createMessageRequest: createMessageRequest,
    getFilterdUserByRequest: getFilterdUserByRequest,
    findRequestByid: findRequestByid,
    changeStatusRequest: changeStatusRequest,
    getAllMessageRequest: getAllMessageRequest,
    getMultipleUser: getMultipleUser,
    updateStatusRequest: updateStatusRequest,
    updateUserRequest: updateUserRequest,
    canOrderDependDeposit: canOrderDependDeposit,
    updateRequestByCountAndFee: updateRequestByCountAndFee,
    getDeposit: getDeposit,
    getMessageRequest: getMessageRequest,
    addOrUpdateFinacialDay: addOrUpdateFinacialDay,
    getPriceByDay: getPriceByDay,
    getAllRequestByStatus: getAllRequestByStatus,
    updateRequestFinal: updateRequestFinal,
    updateAmountUser: updateAmountUser,
    createCheckout: createCheckout,
    updateAmount: updateAmount,
    canOrderDependDepositWithoutRequest: canOrderDependDepositWithoutRequest,
    getAllActiveRequest: getAllActiveRequest,
    addTempValue: addTempValue,
    getLastRequest: getLastRequest,
    revertTempValue: revertTempValue,
    resetTemp: resetTemp,
    updateMessageRequesterSender: updateMessageRequesterSender,
    updateDepositWithZaraz:updateDepositWithZaraz,
    listofAllUser:listofAllUser

};
