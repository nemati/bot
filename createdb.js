const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const constant = require('./const');
const moment = require('moment-timezone');
const utils = require('./utils');
const sequelize = new Sequelize('node', 'root', 'root', {
    define: {
        charset: 'utf8',
        collate: 'utf8_general_ci'
    },
    host: 'localhost',
    dialect: 'mysql',
    logging: false,

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },


    operatorsAliases: false
});


const User = sequelize.define('user', {
    teleid: Sequelize.INTEGER,
    username: Sequelize.STRING,
    first_name: Sequelize.STRING,
    last_name: Sequelize.STRING,
    custom_fname: Sequelize.STRING,
    custom_lname: Sequelize.STRING,
    edit_request: Sequelize.STRING,
    type: Sequelize.STRING,
    phone: Sequelize.STRING,
    credit: Sequelize.STRING,
    usages: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 0},
    tmp_tod: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 0},
    tmp_tom: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 0},
    command: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 0},
    amount: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 0},
    deposit: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 0},
    is_bot: Sequelize.BOOLEAN
});


const Request = sequelize.define('request', {
    teleid_request: Sequelize.INTEGER,
    fee: Sequelize.DOUBLE,
    count: Sequelize.DOUBLE,
    teleid_accept: Sequelize.DOUBLE,
    is_today: Sequelize.BOOLEAN,
    start_time: Sequelize.STRING,
    type: Sequelize.STRING,
    final: Sequelize.DOUBLE,
    edit: {type: Sequelize.BOOLEAN, allowNull: false, defaultValue: false},
    status: {type: Sequelize.STRING, allowNull: false, defaultValue: 'pending'}


});

const MessageRequest = sequelize.define('mreq', {
    teleid: Sequelize.INTEGER,
    message_id: Sequelize.INTEGER,
    request_id: Sequelize.DOUBLE,
    owner: Sequelize.BOOLEAN,
    status: {type: Sequelize.STRING, allowNull: false, defaultValue: 'open'}


});

const FinalPriceDay = sequelize.define('final_price_day', {
    price: Sequelize.DOUBLE,
    day: Sequelize.STRING

});

const Checkout = sequelize.define('checkout', {
    teleid: Sequelize.INTEGER,
    amount: Sequelize.DOUBLE,
    time: Sequelize.STRING,
    status: {type: Sequelize.STRING, allowNull: false, defaultValue: 'open'}

});

sequelize.sync();