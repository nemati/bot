const send = require('./message');
const db = require('./../db');
const constant = require('./../const');
const utils = require('./../utils');
const validation = require('./../validation');


async function accept(bot, msg) {

    msg.data = msg.data.replace("accept", "");

    let request = await db.findRequestByid(msg);

    await validation.timeValidation(bot, msg, request);

    let user = await db.findUserByid(msg);

    await validation.amountValidation(bot, msg, user);


    await canAccept(bot, msg, request);

    const type = request.type === constant.buy ? constant.sell : constant.buy;

    await db.addTempValue(msg, type, request.is_today, request.count * 1000);

    request = await db.changeStatusRequest(msg, constant.close_on);

    let joinUserInfo = await db.getMultipleUser(request);

    await db.updateStatusRequest(msg, constant.close);

    let messageRequests = await db.getAllMessageRequest(msg);

    send.acceptEditMessage(bot, messageRequests, joinUserInfo, request)


}

async function edit(bot, msg) {
    msg.data = msg.data.replace("edit", "");


    let request = await db.findRequestByid(msg);

    await validation.timeValidation(bot, msg, request);

    await canEdit(bot, msg, request, request.count)

    await db.updateCommand(msg, constant.countUpdate);

    await db.updateUserRequest(msg, JSON.stringify({id: msg.data, message_id: msg.message.message_id}))

    send.simpleMessage(bot, msg, "📌📌لطفا مقدار ویرایش شده را نوشته و دکمه ارسال را بزنید📌📌")


}


async function editStep2(bot, msg) {
    await validation.numberValidation(bot, msg);

    let user = await db.findUserByid(msg);

    user.edit_request = JSON.parse(user.edit_request);

    let request = await db.findRequestByid({data: user.edit_request.id});

    await validation.timeValidation(bot, user, request);

    await validation.rangeValidate(bot, msg);

    await canEdit(bot, msg, request, msg.text);


    await db.updateCommand(msg, constant.feeUpdate);

    await db.updateUserRequest(msg, JSON.stringify({
        id: user.edit_request.id,
        message_id: user.edit_request.message_id,
        count: utils.fa2en(msg.text)
    }));

    msg.chat.id = user.teleid;

    msg.message_id = user.edit_request.message_id;

    send.simpleMessage(bot, msg, "‼️مقدار ویرایش شده با موفقیت ثبت شد اکنون قیمت اعلامی ویرایش شده را وارد بفرمایید‼️")

}


async function editFinal(bot, msg) {
    await validation.numberValidation(bot, msg);

    let user = await db.findUserByid(msg);

    user.edit_request = JSON.parse(user.edit_request);


    let request = await db.findRequestByid({data: user.edit_request.id});


    await validation.timeValidation(bot, user, request);

    let diff = await canEdit(bot, msg, request, user.edit_request.count);


    await db.addTempValue(msg, request.type, request.is_today, diff * 1000);

    await db.updateCommand(msg, constant.start);

    request.fee = msg.text;

    request.count = user.edit_request.count;

    request.edit = true;

    await request.save();


    let messageRequest = await db.getMessageRequest(request)

    let requesterMid = await reqeusterMessage(bot, request, messageRequest);


    await db.updateMessageRequesterSender(requesterMid.id, requesterMid.message.message_id);


    guestMessage(bot, request, messageRequest)


}


async function destroy(bot, msg) {
    msg.data = msg.data.replace("destroy", "");

    let request = await db.findRequestByid(msg);

    await validation.timeValidation(bot, msg, request)

    request = await db.changeStatusRequest(msg, constant.destroy)

    await db.revertTempValue(msg, request)

    await db.updateStatusRequest(msg, constant.close)

    let allMessageRequest = await db.getAllMessageRequest(msg)

    send.destroyEditMessage(bot, allMessageRequest)


}


async function canAccept(bot, msg, request) {

    let type = request.type === constant.sell ? constant.buy : constant.sell;
    try {
        await db.canOrderDependDepositWithoutRequest(msg, request.count, type);
        return request
    }
    catch (e) {
        let user = await db.findUserByid(msg);
        send.overTempMessage(bot, msg, user, type)
        throw 'Can not be Accept'

    }


}

async function canEdit(bot, msg, request, editedCount) {

    let diff = getDiffEditedCount(request, editedCount);


    try {
        await db.canOrderDependDepositWithoutRequest(msg, diff, request.type);
        return diff
    }
    catch (e) {
        let user = await db.findUserByid(msg);
        send.overTempMessage(bot, msg, user, request.type)
        throw 'Can not be Accept'

    }


}

function getDiffEditedCount(request, editedCount) {
    if (request.count === editedCount) {
        return 0;
    }


    if (editedCount > request.count)
        return editedCount - request.count
    else
        return request.count - editedCount


}


async function guestMessage(bot, request, messageRequest) {

    for (let i = 0; i < messageRequest.length; i++) {

        if (!messageRequest[i].owner) send.guestMessage(bot, request, messageRequest[i]);


    }
}

async function reqeusterMessage(bot, request, messageRequest) {

    for (let i = 0; i < messageRequest.length; i++) {
        if (messageRequest[i].owner) {

            let requester = await send.requesterMessage(bot, request, messageRequest[i], true);
            requester.id = messageRequest[i].id
            return requester;

        }
    }
}


module.exports = {
    accept: accept,
    destroy: destroy,
    edit: edit,
    editStep2: editStep2,
    editFinal: editFinal

};