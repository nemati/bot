const db = require('./../db');
const constant = require('./../const');
var persianMoment = require('moment-jalaali');
const moment = require('moment-timezone');


function startMessage(bot, msg, text) {
    db.updateCommand(msg, constant.start);
    var opts;


    opts = {
        reply_markup: JSON.stringify({

            keyboard: [
                ['📆 فردایی', '📅 امروزی'],

                ['💲 وجه سود یا ضرر', '💰 وجه تضمین'],
                [' ✏️ویرایش مشخصات', '🔉 راهنما'],
                ['💸 تسویه موقت'],
            ]
        })
    };


    bot.sendMessage(typeof msg.chat !== 'undefined' ? msg.chat.id : msg.message.chat.id, text, opts);


}

function startMessageToAll(bot, user, req, owner) {
    db.updateCommand(msg, constant.start);
    const callBackData = {
        request_id: data.id
    };
    const opts = {
        reply_markup: {
            inline_keyboard: [
                [
                    {
                        text: 'برکت',
                        // we shall check for this value when we listen
                        // for "callback_query"
                        callback_data: callBackData
                    }
                ]
            ]
        }
    };
    var test = 'New Reqeusr';
    text = req.bot.sendMessage(msg.chat.id, text, opts)


}

function editProfileMessage(bot, msg, text) {
    db.updateCommand(msg, constant.start)
    const opts = {
        reply_markup: JSON.stringify({

            keyboard: [
                ['نام خانوادگی 📃', 'نام کوچک 📄'],
                ['شماره حساب 💳', 'شماره موبایل 📱'],
                ['بازگشت به منو اصلی ↪️'],

            ]
        })
    };

    bot.sendMessage(msg.chat.id, text, opts);
}


function adminPanel(bot, msg, text) {
    db.updateCommand(msg, constant.start)
    const opts = {
        reply_markup: JSON.stringify({

            keyboard: [
                ['ثبت قیمت نهایی و محاسبه کل سود و ضرر ها 📃'],
                ['فرستادن پیغام به همه کاربران 📃'],
                ['خروج از مدیریت'],


            ]
        })
    };

    bot.sendMessage(msg.chat.id, text, opts);
}

function simpleMessage(bot, msg, text) {
    return new Promise(function (resolve, reject) {

        try {
            bot.sendMessage(typeof msg.chat !== 'undefined' ? msg.chat.id : msg.message.chat.id, text);
        }
        catch (c) {
            bot.sendMessage(msg.teleid, text)
        }
        resolve('ok')
    });


}

function acceptEditMessage(bot, messageRequest, joinuser, request) {

    var text;
    text = request.type === constant.sell ? '🔴فروشنده :' : '🔵خریدار :';
    text += joinuser.requester.custom_fname === null ? joinuser.requester.first_name : joinuser.requester.custom_fname + " ";
    text += " " + joinuser.requester.custom_lname === null ? " " : joinuser.requester.custom_lname;
    text += '\n';
    text += text = request.type === constant.buy ? '🔴فروشنده :' : '🔵خریدار :';
    text += joinuser.accepter.custom_fname === null ? joinuser.accepter.first_name : joinuser.accepter.custom_fname + " ";
    text += " " + joinuser.accepter.custom_lname === null ? " " : joinuser.accepter.custom_lname;
    text += '\n';
    text += '💸تعداد: ';
    text += request.count;
    text += '\n';
    text += '💵قیمت: ';
    text += request.fee;
    text += '\n';
    text += '⏰نوع معامله: ';
    text += request.is_today ? 'امروزی' : 'فردایی';
    text += '\n';
    text += '🕕تاریخ تسویه: ';
    text += checkoutDay(request);
    text += '\n';
    text += '✅ اتاق معاملات استانبول ✅';
    text = text.replace("null", "");

    for (var i = 0; i < messageRequest.length; i++) {
        const opts = {
            chat_id: messageRequest[i].teleid,
            message_id: messageRequest[i].message_id,
        };

        bot.editMessageText(text, opts);
    }


}

function simpleEditMessage(bot, msg, text) {
    const opts = {
        chat_id: typeof msg.chat !== 'undefined' ? msg.chat.id : msg.message.chat.id,
        message_id: typeof msg.chat !== 'undefined' ? msg.message_id : msg.message.message_id,
    };

    bot.editMessageText(text, opts);
}

function destroyEditMessage(bot, users) {

    return new Promise(function (resolve, reject) {
        for (var i = 0; i < users.length; i++) {

            bot.deleteMessage(users[i].teleid, users[i].message_id)
        }
        resolve(true)
    });


}

function checkoutDay(request) {
    var today = persianMoment();
    if (request.is_today)
        return today.format('jYYYY/jMM/jDD');
    return today.add(1, 'd').format('jYYYY/jMM/jDD')
}


function simpleEditedMessage(bot, msg, text) {
    return new Promise(function (resolve, reject) {
        const opts = {
            chat_id: msg.message.chat.id,
            message_id: msg.message.message_id,
        };

        bot.editMessageText(text, opts);
        resolve('ok')
    });


}

function todayTomorrowMessage(bot, msg, type) {
    const opts = {
        reply_markup: JSON.stringify({

            keyboard: [
                ['فروش ⬆️', 'خرید ⬇️'],
                ['بازگشت به منو اصلی ↪️'],

            ]
        })
    };
    db.updateCommand(msg, type);
    bot.sendMessage(msg.chat.id, 'لطفا از طریق منو گزینه خرید یا فروش را انتخاب کنید', opts);
}


function helpMessage(bot, msg) {
    let text = ' به نام خدا '
    text += '\n';
    text += '\n';
    text += '\n';

    text += ' به اتاق معاملات استانبول خوش آمدید.'

    text += '\n';
    text += '\n';

    text += 'این اتاق در جهت رفاه حال شما عزیزان برای آشنایی و نحوه عملکرد اتاق معاملات استانبول راه اندازی شده است که پس از آموزش و آشنایی با عملکرد اتاق معاملات دوستان می توانند جهت عضویت و انجام معاملات اقدام نماییند.  '
    text += '\n';
    text += '\n';

    // text += 'اتاق معاملات استانبول در تاریخ دوشنبه مورخ۹۷/۹/۱۲  آغاز به کار خواهد کرد.  '
    // text += '\n';
    // text += '\n';

    text += 'عملکرد و نحوه معاملات به شرح زیر می باشد:'
    text += '\n';
    text += '\n';

    text += '۱- معاملات به صورت خرید و فروش امروزی می باشد.'
    text += '\n';
    text += '\n';


    text += '۲- ساعت شروع به کار اتاق ۱۰ صبح و پایان آن ۱۱ شب می باشد. '
    text += '\n';
    text += '\n';
    text += '۳- تسویه اتاق ساعت ۳ بعداظهر میباشد که از ساعت ۱۰ صبح تا ۳ بعداظهر به صورت امروزی و از ساعت ۳ بعداظهر الی ۱۱ شب به صورت فردایی می باشد. '
    text += '\n';
    text += '\n';
    text += '۴- وجه تضمین به ازای هر دلار هزار تومان می باشد. '
    text += '\n';
    text += '\n';
    text += '۵- کمترین لفظ معاملات ۵۰۰۰$ و بیشترین آن ۳۰/۰۰۰$ می باشد که لفظ در قالب بسته های ۵ - ۱۰ - ۱۵ - ۲۰ - ۲۵ - ۳۰ هزار دلاری می باشد. '
    text += '\n';
    text += '\n';
    text += '۶- شخص معامله کننده بر اساس وجه تضمین خود، اجازه معامله و دادن لفظ را دارد. '
    text += '\n';
    text += '\n';
    text += '۷- پس از ساعت ۳ بعداظهر و تسویه اتاق افراد متضرر مجاز به دادن لفظ و انجام معاملات  نخواهند بود و پس از دادن وجه ضرر خود ربات معاملاتی آنها فعال خواهد شد. '
    text += '\n';
    text += '\n';
    text += '۸- افرادی که سود نموده اند پس از ارسال درخواست تسویه موقت در ربات، سود معاملاتی آنها واریز خواهد شد. '
    text += '\n';
    text += '\n';
    text += '۹- نرخها بر اساس کانالهای معتبر و مورد تایید دوستان در اتاق معاملات ارسال می شود. '
    text += '\n';
    text += '\n';
    text += '۱۰- برای شروع معامله وارد ربات زیر شوید و دکمه start را بزنید. '
    text += '\n';
    text += '\n';
    text += '@dollaristanbulbot'
    text += '\n';
    text += '\n';
    text += '۱۱- درصورت نیاز به اطلاعات بیشتر و یا ثبت نام جهت انجام معاملات به ادمین اتاق مراجعه نمایید. '
    text += '\n';
    text += '\n';
    text += '@amirj59tr'
    text += '\n';
    text += '\n';
    text += 'با تشکر گروه معاملات استانبول'
    text += '\n';
    text += '\n';


    bot.sendMessage(msg.chat.id, text);
}

function requesterMessage(bot, request, messageRequest, froceDelete) {
    return new Promise(function (resolve, reject) {

        console.log("Today");
        console.log(request.is_today);
        var text = 'درخواست شما برای ';
        text += request.type === constant.sell ? 'فروش' : 'خرید';
        text += " " + request.count + " دلار به مبلغ " + request.fee + " تومان ";
        text += " "
        text += request.is_today ? "امروزی" : "فردایی";
        text += " "
        text += request.edit === true ? 'اصلاح شد' : 'ثبت شد';
        text += " لطفا منتظر بمانید تا لفظ شما تایید شود";
        var editCommand = "edit" + request.id;
        var destroy = "destroy" + request.id;


        var opts = {
            reply_markup: {
                inline_keyboard: [
                    [

                        {
                            text: 'ابطال',
                            callback_data: destroy
                        },
                        {
                            text: 'ویرایش',
                            callback_data: editCommand
                        },
                    ]
                ]
            }
        };

        if (request.edit === true && froceDelete === true) {
            console.log("Here");
            bot.deleteMessage(messageRequest.teleid, messageRequest.message_id)
        }

         bot.sendMessage(request.teleid_request, text, opts).then(function (msg) {
            var data = {
                message: msg,
                request: request
            };


            resolve(data)

        })


    });

}

function guestMessage(bot, request, messageRequest, cron) {
    return new Promise(function (resolve, reject) {

        let text = request.edit ? '📌اصلاح معامله📌 \n' : '📌درخواست جدید📌 \n';
        text += request.type === constant.sell ? '💰فروش' : '💰خرید';
        text += '\n';
        text += '💸تعداد: ';
        text += request.count;
        text += '\n';
        text += '💵قیمت: ';
        text += request.fee;
        text += '\n';
        text += '⏰نوع معامله: ';
        text += request.is_today ? 'امروزی' : 'فردایی';
        text += '\n';
        text += '🕕تاریخ تسویه: ';
        text += checkoutDay(request);
        text += '\n';
        text += '👍زمان پاسخگویی : کمتر از ';
        text += getSecondTwoTime(request.start_time);
        text += ' ثانیه';
        text += '\n';

        text += '✅ اتاق معاملات استانبول ✅';


        var command = "accept" + request.id;


        var opts = {
            reply_markup: {
                inline_keyboard: [
                    [
                        {
                            text: 'برکت',
                            callback_data: command
                        },

                    ]
                ]
            }
        };
        if (request.edit || cron) {
            opts = {
                reply_markup: opts.reply_markup,
                chat_id: messageRequest.teleid,
                message_id: messageRequest.message_id,
            };

            bot.editMessageText(text, opts).then(function (msg) {
                var data = {
                    message: msg,
                    request: request
                };

                resolve(data)

            })
        }
        else {
            bot.sendMessage(messageRequest.teleid, text, opts).then(function (msg) {
                var data = {
                    message: msg,
                    request: request
                };

                resolve(data)

            })
        }
    });


}

function guestMessageEditTime(bot, user, request) {
    return new Promise(function (resolve, reject) {

        var text = request.edit ? '📌📌اصلاح معامله📌📌 \n' : '📌📌درخواست جدید📌📌 \n';
        text += request.type === constant.sell ? '💰فروش' : '💰خرید';
        text += '\n';
        text += '💸تعداد: ';
        text += request.count;
        text += '\n';
        text += '💵قیمت: ';
        text += request.fee;
        text += '\n';
        text += '⏰نوع معامله: ';
        text += request.is_today ? 'امروزی' : 'فردایی';
        text += '\n';
        text += '🕕تاریخ تسویه: ';
        text += checkoutDay(request);
        text += '\n';
        text += '👍زمان پاسخگویی : کمتر از ';
        text += getSecondTwoTime(request.start_time);
        text += ' ثانیه';
        text += '\n';

        text += '✅ اتاق معاملات استانبول ✅';


        var command = "accept" + request.id;


        var opts = {
            reply_markup: {
                inline_keyboard: [
                    [
                        {
                            text: 'برکت',
                            callback_data: command
                        },

                    ]
                ]
            }
        };

        opts = {
            reply_markup: opts.reply_markup,
            chat_id: user.teleid,
            message_id: user.message_id,
        };

        bot.editMessageText(text, opts).then(function (msg) {
            var data = {
                message: msg,
                request: request
            };

            resolve(data)

        })


    });

}

function overTempMessage(bot, msg, user, type) {
    var text;
    text = 'امکان  ثبت/تایید/ویرایش سفارش بیشتر از وجه اعتبار وجود ندارد.';
    text += '\n';
    text += 'وجه اعتبار کنونی شما مبلغ ';
    let temp = user.tmp_tod + user.tmp_tom;
    text += user.deposit;
    text += ' تومان است';
    text += ' که معادل ';
    text += user.deposit / 1000;
    text += ' دلار است ';
    text += 'تراکنش های شما از مجموع معاملاتی که هنوز تاریخ تسویه آن نرسیده است '
    text += Math.abs(temp);
    text += ' تومان است ';
    text += 'که معادل ';
    text += temp / 1000;
    text += ' دلار ';
    text += ' خواهد بود که  در سیستم شما ';
    if (temp > 0)
        text += 'خرید داشته اید. ';
    else
        text += 'فروش داشته اید. ';
    text += 'بدیهی است برای ';
    if (type === constant.sell)
        text += 'فروش';
    else
        text += 'خرید';
    text += ' نیاز است یکی از کارهای زیر را انجام دهید\n';
    text += '📌وجه اعتبار خود را افزایش دهید\n';
    text += '📌اقدام به ';
    if (temp > 0)
        text += 'فروش';
    else
        text += 'خرید';
    text += ' کنید\n';
    text += '📌مبلغ دیگری را وارد نمایید\n';
    simpleMessage(bot, msg, text);

}


function getSecondTwoTime(end) {
    var now = moment(moment().tz(constant.tehran).format());
    var end = moment.duration(now.diff(moment(end)));
    return 60 - parseInt(end.asSeconds());


}


function backMessage(bot, msg, text) {
    const opts = {
        reply_markup: JSON.stringify({

            keyboard: [

                ['بازگشت به منو اصلی ↪️']

            ]
            ,
            resize_keyboard: true,
            one_time_keyboard: true
        })
    };

    bot.sendMessage(msg.chat.id, text, opts);
}


function todayResultRequest(bot, ids, hashmap, amount) {

    for (var i = 0; i < ids.length; i++) {

        var data = hashmap.get(ids[i]);


        var text = '📌📌 گزارش معامله های امروز شما 📌📌';
        text += '\n';
        text += '💲 قیمت پایه :';
        text += amount;
        text += '\n';
        text += '📜 تعداد کل معامله ها :';
        text += data.count;
        text += '\n';
        text += '🔵 مجموع کل سود :';
        text += data.sod;
        text += '\n';
        text += '🔴 مجموع کل ضرر : ';
        text += data.zarar;
        text += '\n';
        text += '🔷 مجموع کارمزد : ';
        text += data.wage;
        text += '\n';
        text += '✅ اتاق معاملات استانبول ✅';
        bot.sendMessage(ids[i], text);


    }
}

function simpleDestoryMessage(bot, msg) {
    let text = 'متاسفانه زمان معامله به اتمام رسیده است و معامله به صورت خودکار به حالت ابطال در آمده است';
    text += '\n';
    text += "توجه زمان برکت و ویرایش  تنها یک دقیقه است.";
    simpleMessage(bot, msg, text);
}


module.exports = {
    startMessage: startMessage,
    simpleMessage: simpleMessage,
    todayTomorrowMessage: todayTomorrowMessage,
    editProfileMessage: editProfileMessage,
    backMessage: backMessage,
    requesterMessage: requesterMessage,
    guestMessage: guestMessage,
    simpleEditedMessage: simpleEditedMessage,
    acceptEditMessage: acceptEditMessage,
    destroyEditMessage: destroyEditMessage,
    simpleEditMessage: simpleEditMessage,
    todayResultRequest: todayResultRequest,
    adminPanel: adminPanel,
    guestMessageEditTime: guestMessageEditTime,
    overTempMessage: overTempMessage,
    simpleDestoryMessage: simpleDestoryMessage,
    helpMessage: helpMessage


};

