require('dotenv').config()
require('./utils').initOs();
const express = require('express');
const app = express();
const shell = require('shelljs');

const port = process.env.DEBUG === 'true' ? process.env.LOCAL_PORT : process.env.SERVER_PORT;


app.get('/gitlab', function (req, res) {
    runShell(res)

});


app.post('/gitlab', function (req, res) {
    runShell(res)

});

app.get('/', function (req, res) {
    console.log('Gitlab WebHook Called');
    res.send('All done');

});


app.post('/', function (req, res) {
    console.log('Gitlab WebHook Called');
    res.send('All done');

});


app.listen(port, () => {
    console.log(`Express server is listening on ${port}`);
});


async function runShell(res) {
    await console.log('Gitlab WebHook Called');
    await res.send('All done');
    await shell.exec('./run.sh')
}