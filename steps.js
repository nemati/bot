const db = require('./db');
const send = require('./ui/message');
const validation = require('./validation');
const constant=require('./const');


async function step1(bot, msg, type, isToday, command) {
    await validation.numberValidation(bot, msg);

    await validation.rangeValidate(bot, msg);


    await canAccept(bot, msg, type);


    await db.createNewRequest(msg, type, isToday);


    await db.updateCommand(msg, command);


    send.backMessage(bot, msg, 'مقدار با موفقیت ثبت شد اکنون قیمت اعلامی را وارد بفرمایید')

}

async function step2(bot, msg, type, isToday) {

    await validation.numberValidation(bot, msg);


    let request = await canAccept(bot, msg, type, isToday);


    await db.updateCommand(msg, constant.start);

    await db.addTempValue(msg, type, isToday, request.count * 1000);

    request = await db.updateRequest(msg, type, isToday);

    let telegramRequesterMid = await send.requesterMessage(bot, request);

    await db.createMessageRequest(telegramRequesterMid);

    let filterUsers = await db.getFilterdUserByRequest(msg, request, type);

    let telegramGuestUserMessageIDs = await guestMessage(bot, request, filterUsers);

    for (let i = 0; i < telegramGuestUserMessageIDs.length; i++)
        db.createMessageRequest(telegramGuestUserMessageIDs[i])

}


async function guestMessage(bot, request, users) {

    let promiseUser = [];

    for (let i = 0; i < users.length; i++) {
        let mId = await send.guestMessage(bot,  request,users[i]);
        promiseUser.push(mId);
    }

    return await Promise.all(promiseUser);

}


async function canAccept(bot, msg, type, isToday) {
    let request;
    if (isToday !== undefined)
        request = await db.getLastRequest(msg, type, isToday);
    try {
        await db.canOrderDependDepositWithoutRequest(msg, isToday === undefined ? msg.text : request.count, type);
        return request;
    }
    catch (e) {
        let user = await db.findUserByid(msg);
        send.overTempMessage(bot, msg, user, type)
        throw 'Temp is not Accept'
    }


}


module.exports = {
    step2: step2,
    step1: step1
};