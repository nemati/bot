const constant = require('./const');
const moment = require('moment-timezone');

function checkUserCanTransaction(user, amount, type) {

    if (type === constant.sell) {
        amount = amount * -1;
        user.deposit = user.deposit * -1;


    }




    if (type === constant.buy) return (user.tmp_tom + user.tmp_tod + amount) <= user.deposit;
    else return (user.tmp_tom + user.tmp_tod + amount) >= user.deposit


}

function initOs() {
    if (process.platform === 'darwin' || process.platform === 'win32')
        process.env.DEBUG = 'true';
    else
        process.env.DEBUG = 'false'
}

function fa2en(txt) {
    return txt.replace(/۱/g, "1").replace(/۲/g, "2").replace(/۳/g, "3").replace(/۴/g, "4").replace(/۵/g, "5").replace(/۶/g, "6").replace(/۷/g, "7").replace(/۸/g, "8").replace(/۹/g, "9")
        .replace(/۰/g, "0");

}

function isTimeOtherOneMin(end) {
    var now = moment(moment().tz(constant.tehran).format());
    var end = moment.duration(now.diff(moment(end)));
    if (parseInt(end.asMinutes()) >= 1) return true;
    else return false;


}


module.exports = {

    checkUserCanTransaction: checkUserCanTransaction,
    initOs:initOs,
    fa2en:fa2en,
    isTimeOtherOneMin:isTimeOtherOneMin


};


