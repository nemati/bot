const db = require('./db');
const send = require('./ui/message');
const utils = require('./utils');
const constant = require('./const');

function checkoutValidation(bot, msg) {
    return new Promise(function (resolve, reject) {
        db.findUserByid(msg).then((data) => {
            if (data.amount === 0) {
                send.simpleMessage(bot, msg, "مقدار اعتبار شما صفر است!");
                reject("amount === 0")
            }

            else if (data.amount < 0) {
                send.simpleMessage(bot, msg, "لطفا وجه اعتبار ضرری خود را پرداخت کنید!!!");
                reject("data.amount <0")
            }
            else {
                var text = "ثبت سفارش تسویه به مبلغ ";
                text += data.amount;
                text += " تومان ";
                text += "با موفقیت انجام شد سفارش تسویه شما ظرف 24 ساعت آینده پرداخت خواهد شد.";
                send.simpleMessage(bot, msg, text);
                resolve(data.amount)
            }

        })
    });

}

async function numberValidation(bot, msg) {

    const st = utils.fa2en(msg.text);
    const number = st;
    let phoneRGEX = RegExp('^[1-9]\\d*$');
    let phoneResult = phoneRGEX.test(number);
    if (phoneResult === false) {
        send.simpleMessage(bot, msg, 'عبارت وارد شده معتبر نمیباشد');
        throw 'Error Validation'
    }


}

function rangeValidate(bot, msg) {

    if (
        msg.text === '5000' ||
        msg.text === '10000' ||
        msg.text === '15000' ||
        msg.text === '20000' ||
        msg.text === '25000' ||
        msg.text === '30000' ||
        msg.text === '35000' ||
        msg.text === '40000' ||
        msg.text === '45000' ||
        msg.text === '50000' ||
        msg.text === '55000' ||
        msg.text === '60000' ||
        msg.text === '65000' ||
        msg.text === '70000' ||
        msg.text === '75000' ||
        msg.text === '80000' ||
        msg.text === '85000' ||
        msg.text === '90000' ||
        msg.text === '95000' ||
        msg.text === '100000' ||
        msg.text === '105000')
        return
    else {
        send.simpleMessage(bot, msg, 'مقدار دلار بایستی یکی از رقم های ۵۰۰۰ ، ۱۰۰۰۰ ، ۱۵۰۰۰ ، ۲۰۰۰۰ ، ۲۵۰۰۰ ،  ۳۰۰۰۰ دلار باشد. ');
        throw 'Range Validation Problem'
    }


}

async function timeValidation(bot, msg, request) {

    switch (request.status) {
        case constant.pending:
            if (utils.isTimeOtherOneMin(request.start_time)) {
                await db.revertTempValue(msg, request);
                await send.simpleDestoryMessage(bot, msg);
                await db.changeStatusRequest({data: request.id}, constant.timeout);
                let users = await db.getAllMessageRequest({data: request.id});
                send.destroyEditMessage(bot, users);
                throw "Time Out"
            }
            return;


        case constant.destroy:
            send.simpleMessage(bot, msg, "متاسفانه این معامله حذف شده است");
            throw "request destroy";
        case constant.close_off:
            send.simpleMessage(bot, msg, "متاسفانه این معامله بسته  شده است");
            throw "request close_off";

        case  constant.close_on:
            send.simpleMessage(bot, msg, "متاسفانه این معامله بسته  شده است");
            throw "request close_on";
        case  constant.timeout:
            send.simpleMessage(bot, msg, "متاسفانه این معامله بسته  شده است");
            throw "request timeout";


    }


}

function amountValidation(bot, msg, user) {
    if (user.amount < 0) {
        send.simpleMessage(bot, msg, 'تا زمان تصفیه وجه ضرری خود امکان شرکت در اتاق معاملات را ندارید');
        throw 'checkout zarar'
    }
}

module.exports = {
    checkoutValidation: checkoutValidation,
    numberValidation: numberValidation,
    rangeValidate: rangeValidate,
    timeValidation: timeValidation,
    amountValidation: amountValidation


};
