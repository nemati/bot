var db = require('./db');
var moment = require('moment-timezone');
var persianMoment = require('moment-jalaali');
const constant = require('./const');
const HashMap = require('hashmap');
var dash = require('lodash/collection');
var send = require('./ui/message');


function doFinancialJob(bot, msg, amount) {
    db.getAllRequestByStatus(constant.close_on).then((data) => {

        analyseRequest(bot, data, amount);
        send.simpleMessage(bot, msg, 'با موفقیت کل سود ها و ضرر ها محاسبه شد')
    });
}


async function analyseRequest(bot, data, amount) {
    var map = new HashMap();
    var ids = [];

    for (var i = 0; i < data.length; i++) {

        if (isAfter3(data[i].start_time) || (isToday(data[i].start_time) && data[i].is_today) ||
            (!isToday(data[i].start_time) && !data[i].is_today)) {
            var diff = (data[i].fee * data[i].count) - (amount * data[i].count);
            var wage = data[i].count * 5;


            await db.updateAmountUser(data[i].teleid_request, diff - wage);
            await db.updateAmountUser(data[i].teleid_accept, (-1 * (diff)) - wage);
            await db.updateRequestFinal(data[i].id, constant.close_off, diff);
            if (!dash.includes(ids, data[i].teleid_request)) ids.push(data[i].teleid_request);
            if (!dash.includes(ids, data[i].teleid_accept)) ids.push(data[i].teleid_accept);
            if (typeof map.get(data[i].teleid_request) === 'undefined') {
                map.set(data[i].teleid_request, {
                    sod: diff > 0 ? diff : 0, zarar: diff < 0 ? -1 * (diff) : 0, count: 1,
                    wage: wage
                })
            }
            else {
                var tmp = map.get(data[i].teleid_request);
                map.set(data[i].teleid_request
                    , {
                        sod: diff > 0 ? tmp.sod + diff : tmp.sod,
                        zarar: diff < 0 ? -1 * (diff) + tmp.zarar : tmp.zarar,
                        count: tmp.count + 1, wage: tmp.wage + wage
                    })
            }
            diff = -1 * (diff);

            if (typeof map.get(data[i].teleid_accept) === 'undefined') {
                map.set(data[i].teleid_accept, {
                    sod: diff > 0 ? diff : 0, zarar: diff < 0 ? -1 * (diff) : 0, count: 1,
                    wage: wage
                })
            }
            else {
                var tmp = map.get(data[i].teleid_accept);
                map.set(data[i].teleid_accept
                    , {
                        sod: diff > 0 ? tmp.sod + diff : tmp.sod,
                        zarar: diff < 0 ? -1 * (diff) + tmp.zarar : tmp.zarar,
                        count: tmp.count + 1, wage: tmp.wage + wage
                    })
            }


        }


    }
    for (var i = 0; i < ids.length; i++) {

        var d = map.get(ids[i]);


        if (d.zarar !== 0) {
            if (d.zarar < 0)
                d.zarar = -1 * d.zarar
            await db.updateDepositWithZaraz(ids[i], d.zarar)

        }


    }

    await db.resetTemp();
    send.todayResultRequest(bot, ids, map, amount)


}

function isToday(day2) {
    var day = moment(day2);
    var now = moment();
    var end = moment.duration(now.diff(day));

    if (parseInt(end.asDays()) === 0) return true;
    else return false;

}

function isAfter3(day2) {
    var day = moment(day2).tz(constant.tehran);

    return day.format("H") >= 15

}


module.exports = {
    doFinancialJob: doFinancialJob

};
