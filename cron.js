require('dotenv').config();
require('./utils').initOs();
var cron = require('node-cron');
const TelegramBot = require('node-telegram-bot-api');
const token = process.env.DEBUG === 'true' ? process.env.LOCAL_TOKEN : process.env.SERVER_TOKEN;
const bot = new TelegramBot(token, {polling: false});
const db = require('./db');
const moment = require('moment-timezone');
const constant = require('./const');
const send = require('./ui/message');

cron.schedule('*/1 * * * * *', () => {


    db.getAllActiveRequest().then((requests) => checkTimeout(requests))
});


async function checkTimeout(requests) {

    for (var i = 0; i < requests.length; i++) {
        if (isTimeOtherOneMin(requests[i].start_time)) {
           let req = await db.findRequestByid({data: requests[i].id});
            if (req.status !== constant.pending)
                continue;
            await db.revertTempValue({data: requests[i].id}, requests[i]);
            removeMessage({data: requests[i].id});

        }

        else
            await updateUserMessage(requests[i])
    }


}

async function updateUserMessage(request) {
    let messageRequest = await db.getMessageRequest(request)
    guestMessage(bot, request, messageRequest);
}


async function guestMessage(bot, request, messageRequest) {

    for (let i = 0; i < messageRequest.length; i++) {

        if (!messageRequest[i].owner) send.guestMessage(bot, request, messageRequest[i],true);


    }
}


function removeMessage(msg) {
    db.changeStatusRequest(msg, constant.timeout).then(() => {
        return db.getAllMessageRequest(msg)
    }).then((allUsers) => {

        send.destroyEditMessage(bot, allUsers)
    })
}

function isTimeOtherOneMin(end) {
    var now = moment(moment().tz(constant.tehran).format());
    var end = moment.duration(now.diff(moment(end)));
    if (parseInt(end.asMinutes()) >= 1) return true;
    else return false;


}